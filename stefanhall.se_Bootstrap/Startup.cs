﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(stefanhall.se_Bootstrap.Startup))]
namespace stefanhall.se_Bootstrap
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
